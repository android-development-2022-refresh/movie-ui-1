---
title: Basic Navigation
template: main-repo.html
---

Let's start setting up the navigation. 

We define `ActorDisplay` and `MovieDisplay` as placeholder screens for now. They only display the `id` of the actor or movie, as we don't yet have support in the data layer to fetch actors or movies.

We modify `ActorList` and `MovieList` to pass in `onXXXClick` functions, and set the `SimpleText` to call them when the user clicks them. We do similarly in `RatingDisplay` - when one of its displayed movies is clicked, we'll call `onMovieClick`. These calls notify the caller that something is being clicked. The caller will deal with the actual navigation.

That happens in `TestScreen`, which is our top-level composable that works with the view model. We define navigation and screen selection inside it. There are several changes in this file:

   * We add a `BackHandler`. This sets up _scoped_ back-button handling for this composable and each called composable. A called composable (at any depth) can override the `BackHandler` if needed. Our `BackHandler` is simple - we tell the view model to pop a screen from the stack.

   * We change the `switchTo` calls to `setScreenStack` or `pushScreen`, depending on the needed navigation flow. (I've also removed `switchTo` from the view model, as it's no longer needed.)

   * When looking at the `viewModel.screen`, if it's `null`, we need to exit the application. To do this, we pass an `onExit` function to `TestScreen`, and call it if we see a `null` screen.

   * We add more clauses to our `when` expression for the newly-added screens.

   * Inside that `when`, we define what navigation happens. Each screen tells us when things are clicked; the screens don't know anything about navigation, which makes them more reusable. We call `pushScreen` when the user is clicking on items in each screen.

Finally, our `MainActivity` needs to deal with exiting. We pass a lambda to `TestScreen` for its `onExit` parameter. This lambda just calls `finish()`, which tells Android that it should close out the `MainActivity`. Because there are no more Android-tracked Activities, the application ends.

!!! note

    In case you're confused by 

    ```kotlin
    TestScreen {
        finish() // handle "onExit"
    }
    ```

    this is equivalent to

    ```kotlin
    TestScreen(
        onExit = {
            finish() // handle "onExit"
        }
    )
    ```

    If the _last_ parameter to a Kotlin function is a function, you can move it _out_ of the parentheses. If that parameter is the _only_ parameter to the function, you can also remove the parentheses.

    If we had another parameter to a function, like

    ```kotlin
    fun someFunction(
        x: Int,
        onFoo: () -> Unit,
    ) {
        ...
    }
    ```

    You would call it as

    ```kotlin
    someFunction(42) {
       ... 
    }
    ```

At this point, we can walk through the navigation (except for moving between actors and movies - we don't have that support yet in the data layer).