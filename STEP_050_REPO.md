---
title: Actors <-> Movies
template: main-repo.html
---

The `Actor` <-> `Movie` relationship is many-to-many, and we store the mapping in `Role` entities. At its most basic, we could define an `ActorWithMovies` as

```kotlin
data class ActorWithMovies(
    @Embedded
    val actor: Actor,

    @Relation(
        parentColumn = "id",
        entityColumn = "id",
        associateBy = Junction(
            Role::class,
            parentColumn = "actorId",
            entityColumn = "movieId",
        ),
    )
    val movies: List<Movie>,
)
```

This can be used in a DAO function like

```kotlin
@Transaction
@Query("SELECT * FROM Actor WHERE id = :id")
abstract suspend fun getActorWithMovies(id: String): ActorWithMovies
```

But there's a problem; we _only_ get a list of `Movies`. We don't get the _associative data_, the character name or order in credits, that's stored in `Role`.

To make that work, we need to break up the data class that we use to fetch the data into _two_ data classes (in each direction). 

From the `Actor` side, we have

```kotlin
data class ActorWithFilmography(
    @Embedded
    val actor: Actor,

    @Relation(
        entity = Role::class,
        parentColumn = "id",
        entityColumn = "actorId",
    )
    val filmography: List<RoleWithMovie>,
)

data class RoleWithMovie(
    @Embedded
    val role: Role,

    @Relation(
        parentColumn = "movieId",
        entityColumn = "id"
    )
    val movie: Movie,
)
```

Start looking from `RoleWithMovie`. Room fills this `data class` when a `Role` is fetched. The `Role` is held as the `@Embedded role` property, and the `@Relation` asks Room to fetch exactly one `Movie` whose `id` matches the `movieId` in the `Role`.

We _could_ (but we won't) create a DAO function like

```kotlin
@Transaction
@Query("SELECT * FROM Role WHERE actorId = :id")
abstract suspend fun getRolesWithMoviesForActor(id: String): List<RoleWithMovie>
```

This would fetch all `Roles` with the specified `actorId`, run a secondary query to fetch the `Movies` associated with those `Roles`, then create a `RoleWithMovie` for each.

Keep this in mind for the next step...

Now, look at `ActorWithFilmography`. Note how it pulls in `RoleWithMovie`.

By writing a DAO function:

```kotlin
@Transaction
@Query("SELECT * FROM Actor WHERE id = :id")
abstract suspend fun getActorWithFilmography(id: String): ActorWithFilmography
```

We ask Room to

   * Find the `Actor` with the given id
   * Run another query because of the `@Relation` in `ActorWithFilmography` that will fetch all `Roles` for the Actor (the `@Relation` specifies the `Role` as its `entity`) and create `RoleWithMovie` instances for each.
   * To fetch those `Roles` and `RoleWithMovie`, Room basically does the same thing it would have done for `getRolesWithMoviesForActors` (calling a _third_ query in the process)

We do similar for `MovieWithCast`.

!!! note

    The above starts getting rather complex. Right now, this is the only way to get associative data that's attached to a many-to-many relationship. You may need to do this at some point, but the docs don't cover this scenario, which is why I wanted to feature it. (I need to write a blog post on this...)

The rest of these changes are exposing the functionality through the repository and view model, then setting up the screens to display the data and handle navigation. We set up the `MovieDisplay` and `ActorDisplay` similar to how we set up `RatingDisplay`.


We now have our navigation and data working.

But the screens are **very** rough...
