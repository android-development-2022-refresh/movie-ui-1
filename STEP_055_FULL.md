---
title: Scaffold - A Slotted API
template: main-full.html
---

A key component of a consistent user interface is management of its overall structure. This includes

   * App Bar (aka Tool Bar) at the top of the UI
   * Navigation Drawer
   * Bottom Navigation
   * Floating Action Button
   * Snackbar
   * Body Content

Jetpack Compose defines an elegant and flexible solution in its `Scaffold` composable function.

`Scaffold` manages sections of the screen that it calls **Slots**. Each **Slot** is simply a @Composable lambda function that you pass into `Scaffold` and it manages their size and visibility. `Scaffold` acts as a dynamic layout manager, responding to certain user input (such as dragging from the edge to open the navigation drawer) to manage its content.

A **Slotted** API is very flexible; you can emit _whatever_ nodes you would like in those slots.

An example of `Scaffold` in action (with some details elided):

```kotlin
Scaffold(
    topBar = {
        TopAppBar(
            title = {
                Text(...My Application...)
            },
            actions = {
                IconButton(onClick = { ... }) {
                    Icon(...add...)
                }
                IconButton(onClick = { ... }) {
                    Icon(...delete...)
                }
            }
        )
    }
) { paddingValues ->
    Column(modifier = Modifier.padding(paddingValues)) {
        Text(...Name...)
        Text(...Scott...)
        Text(...Age...)
        Text(...55...)
    }
}
```

Here we use `Scaffold's` `topBar` and `content` slots. (`content` is the lambda that's outside its parentheses). 

In the `topBar` slot, we use a `TopAppBar`, which is another **slotted** function. We fill in its `title` and `actions` slots. The `TopAppBar` positions the title and actions for us.

We fill in the `content` slot with a nice little name-and-address form.

This results in a UI that looks like

![Scaffold Example](screenshots/scaffold.png)

If we have common actions and/or other features such as buttons on a bottom bar, we may want to define our own Scaffold function:

```kotlin
@Composable
fun MyScaffold(
    title: String,
    content: @Composable (PaddingValues) -> Unit, // our content slot
) =
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = title)
                },
                actions = {
                    // common actions
                }
            )
        }
    ) { paddingValues ->
        content(paddingValues)
    }
```

We set up a common, fixed app bar at the top and pass a modifier to our content slot

which we can use as follows

```kotlin
MyScaffold(title = "My Application") { modifier ->
    Column(modifier = Modifier.padding(paddingValues)) {
        Text(...Name...)
        Text(...Scott...)
        Text(...Age...)
        Text(...55...)
    }        
}
```

Creating composable functions like this acts similar to _subclassing_ in other UI frameworks.
