---
title: Application Navigation
template: main-full.html
---

Our movie application has a simple navigation flow. 

![Navigation Flow](navigation.drawio.svg)

At the bottom of the UI are three navigation tabs:

   * Ratings
   * Movies
   * Actors

Clicking one of them jumps to the list for that type of data, _and_ clears the navigation stack. It's like starting the navigation flow over at one of the list screens.

Initially, the _Movie List_ is displayed.

Each list screen displays a list of all items of that type. When you click on one of the items, you'll be taken to the display screen for that data.

Each data display shows a list related data:

   * Rating Display shows a list of Movies with that Rating
   * Movie Display shows a list of actors who appeared in the Movie
   * Actor Display shows a list of movies in which the actor appeared

Clicking on the related info will take you to that related item.

The user can drill down as deeply as they would like; each item visited is pushed on a stack. Pressing the Android back button will take them to the previously-visited item. If they're on a top-level list screen, pressing back will exit the application.

To represent screens, we'll use a Kotlin **sealed interface** and implementations:

```kotlin
sealed interface Screen

object RatingList: Screen
object MovieList: Screen
object ActorList: Screen

data class RatingScreen(
    val id: String
): Screen
data class ActorScreen(
    val id: String
): Screen
data class MovieScreen(
    val id: String
): Screen
```

Android provides a Navigation Component, but, we'll keep things simple for now and just use a stack that we manage.