---
title: UI From the Outside 
template: main-repo.html
---

Let's make our screens look nicer and include common functionality.

Overall, we'd like something like

```kotlin
@Composable
fun MovieScaffold(
    title: String,
    onSelectListScreen: (Screen) -> Unit, 
    onResetDatabase: () -> Unit,
    content: @Composable (PaddingValues) -> Unit,
) {
    Scaffold(
        topBar = {
            // title and reset-database action
        },
        content = { paddingValues ->
            content(paddingValues)
        },
        bottomBar = {
            // buttons for each list screen
            //    if pressed, call onSelectListScreen
        }
    )
}
```

We use this as the basis for every screen in the application. 

!!! note

    It's time to talk about externalizing your strings!

    Any literal string that will appear in the user interface should be externalized. You typically define strings in `src/main/res/values/strings.xml` inside the module that needs them. (You can access strings from modules you depend upon as well). This allows the strings to be translated for other languages/regions, or changed based on other configurations. For example, if you have a particularly small screen, you may want to use shorter strings.

    ```xml
    <resources>
        <string name="screen_title_ratings">MPAA Ratings</string>
        <string name="screen_title_movies">Movies</string>
        <string name="screen_title_actors">Actors</string>
    </resources>
    ```

    You can then reference them in your composable function by calling `stringResource`:
    
    ```kotlin
    stringResource(R.string.screen_title_ratings)
    ```

    Sometimes I'll define helper functions for literal text:

    ```kotlin
    @Composable
    fun Label(
        @StringRes textId: Int
    ) =
        Text(
            text = stringResource(textId),
            style = MaterialTheme.typography.h5,
            modifier = Modifier.padding(8.dp)
        )
    ```

    Note the `@StringRes` annotation. The Android lint checker defines a rule that the caller must pass a proper string resource id (`R.string.xxx`) to parameters annotated with `@StringRes`. Use that annotation if you're creating a function that expects a string resource id.

    As part of this step, I've externalized all literal strings.


!!! note

    About those icons...

    Jetpack Compose comes with a basic set of icons, but sometimes you need more.

    I added in dependency

    ```groovy
    implementation 'androidx.compose.material:material-icons-extended:1.2.1'
    ```

    This is a _huge_ dependency, and should not be included in release apps. In the next section, we'll see how we can pull out the icons we need.