---
title: Screen State
template: main-repo.html
---

I like to add the screen state and its management early on. This allows me to create placeholder screens and navigate between them to ensure my navigation flow feels ok.

## Screen Objects/Classes

To start, we flesh out the `Screen` objects and classes. I've renamed `RatingScreen` to `RatingDisplay` to be more explicit about its purpose, and follow the same convention when defining displays for a movie and actor.

The "List" screen representations don't need to hold any data; their screens display _all_ of their data. We therefore don't need to ever create more than one instance of these, and can represent these as singletons using Kotlin the `object` keyword.

The "Display" screen representations need to know which specific item they're displaying, so we hold the `id`. This requires separate screen instances for each `id` we visit, so we'll use a `data class` when defining them.

## Screen State Management

We'll track which screen we're on inside the view model. We had already added

```kotlin
var screen by mutableStateOf<Screen?>(MovieList)
    private set
```

to track the _current_ screen, but we also need a stack to allow the user to return to previously-visited screens.

We define

```kotlin
private var screenStack: List<Screen> = listOf(MovieList)
    set(value) {
        field = value
        screen = value.lastOrNull()
    }
```

This `screenStack` property holds an immutable list of `Screens`. The last `Screen` in the list is the currently-displayed one. If the list is empty, we'll treat that as an "exit the application" state, to be handled by the `Activity`.

!!! note

    Lists created by `listOf` _can_ actually be modified by casting them to a `MutableList`. Unfortunate... But we're managing it internally and never changing it ourselves.

Here we take advantage of a custom `set` function for the property. Whenever the stack changes, we'll automatically update the `screen` property. Because the `screen` property is a Compose `State`, Compose will trigger recomposition (if its value has changed).

Next we expose functions that the UI can use to direct our navigation.

   * `pushScreen` - creates a new `screenStack` by appending the specified `Screen`

   * `popScreen` - creates a new `screenStack` by removing the last `Screen` (if any were present)

   * `setScreenStack` - creates a new, single-`Screen` stack
