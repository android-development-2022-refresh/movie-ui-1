---
title: Copying Extended Icons 
template: main-repo.html
---

In the last step, we used a few Icons by referencing

```kotlin
Icons.Default.Emergency
Icons.Default.Movie
Icons.Default.Person
```

These are `ImageVectors`, objects that declare how to show an icon using Scalable Vector Graphics (SVG) paths.

!!! info 

    To explore a list of available icons (many in the extended icon set), see [https://fonts.google.com/icons?selected=Material+Icons](https://fonts.google.com/icons?selected=Material+Icons)


Jetpack Compose includes a library of basic icons, but they're often not enough. If you can't find the icon you're looking for, add this dependency to your `app/build.gradle`:

```groovy
implementation 'androidx.compose.material:material-icons-extended:1.2.1'
```

!!! caution

    This is a _large_ dependency and can greatly impact debug build time. (If you have proguard minimization turned on, it should remove unused icons in your release build, but debug will be a good bit slower)

    You should copy the icons you need from it.

Once you find and test the icons you need, you can copy them to your project:

Find the referenced icon

```kotlin
Icon(
imageVector = Icons.Default.Add, // An Icon!!!
contentDescription = stringResource(id = R.string.add)
)
```

Control-click the icon name (`Add` in this example) to see its `ImageVector` source:

![ImageVector Source](screenshots/icon2.png)

Press the "select opened file" icon in the Project View. It looks like a target. You can also press ++alt+f1++ then press ++1++ to open the file 

If the file appears under "material-icons-core", you don't need to copy it. If it's under "material-icons-extended", you should

   1. Copy the package name from the `package` line in the source
   2. Right-click `app/src/main/java` or `app/src/main/kotlin` and chose "New->Package"
   3. Paste the package name and press enter
   4. Create a new file named `IconName.kt`
   5. Copy and paste the icon source into the new file

After you've copied all external icons, you can delete the "material-icons-extended" dependency.
