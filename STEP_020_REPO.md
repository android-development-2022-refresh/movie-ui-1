---
title: Project Setup
template: main-repo.html
---

We start with the "Empty Compose Activity" project wizard.

Android Studio Dolphin just came out. We update the version number of the Android Gradle Plugin (AGP) to the latest and greatest.

