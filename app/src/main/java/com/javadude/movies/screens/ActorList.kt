package com.javadude.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies.R
import com.javadude.movies.Screen
import com.javadude.movies.components.MovieScaffold
import com.javadude.movies.components.SimpleText
import com.javadude.movies.repository.ActorDto

@Composable
fun ActorList(
    actors: List<ActorDto>,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onActorClick: (String) -> Unit,
) = MovieScaffold(
    title = stringResource(id = R.string.screen_title_actors),
    onSelectListScreen = onSelectListScreen,
    onResetDatabase = onResetDatabase,
) { paddingValues ->
    Column(modifier = Modifier.padding(paddingValues)) {
        actors.forEach {
            SimpleText(text = it.name) {
                onActorClick(it.id)
            }
        }
    }
}
