package com.javadude.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies.R
import com.javadude.movies.Screen
import com.javadude.movies.components.MovieScaffold
import com.javadude.movies.components.SimpleText
import com.javadude.movies.repository.RatingDto

@Composable
fun RatingList(
    ratings: List<RatingDto>,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onRatingClick: (String) -> Unit,
) = MovieScaffold(
    title = stringResource(id = R.string.screen_title_movies),
    onSelectListScreen = onSelectListScreen,
    onResetDatabase = onResetDatabase,
) { paddingValues ->
    Column(modifier = Modifier.padding(paddingValues)) {
        ratings.forEach {
            SimpleText(text = it.name) {
                onRatingClick(it.id)
            }
        }
    }
}