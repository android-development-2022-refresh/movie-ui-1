package com.javadude.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies.R
import com.javadude.movies.Screen
import com.javadude.movies.components.MovieScaffold
import com.javadude.movies.components.SimpleText
import com.javadude.movies.repository.MovieDto

@Composable
fun MovieList(
    movies: List<MovieDto>,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onMovieClick: (String) -> Unit,
) = MovieScaffold(
    title = stringResource(id = R.string.screen_title_movies),
    onSelectListScreen = onSelectListScreen,
    onResetDatabase = onResetDatabase,
) { paddingValues ->
    Column(modifier = Modifier.padding(paddingValues)) {
        movies.forEach {
            SimpleText(text = it.title) {
                onMovieClick(it.id)
            }
        }
    }
}