---
title: Wrap Up
template: main-full.html
---

Things are looking and working much better! We've got a common wrapper around the guts of each page, and we're pulling the right data.

But there's more to do:

   * Convert our list displays to be more dynamic and efficient
   * Display items in a list as cards
   * Allow the user to select items in a list and delete them
   * Edit data

We'll work on this in a later module.